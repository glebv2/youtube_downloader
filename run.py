import gi
from pytube import *

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Notify


class main(Gtk.Window):
    def __init__(self):
        super().__init__(title="Youtube Downloader")
        box = Gtk.Box(spacing=2)
        self.add(box)
        self.entry = Gtk.Entry()
        button1 = Gtk.Button("Enter")
        button1.connect("clicked", self.settext)
        box.pack_start(button1, True, True, 0)
        box.pack_start(self.entry, True, True, 0)

    def settext(self, widget):
        url = self.entry.get_text()
        YouTube(url).streams.first().download()
        Notify.init("Youtube Downloader")
        Notify.Notification.new("File Downloaded!").show()
        win1 = done()
        win1.connect("destroy", Gtk.main_quit)
        win1.show_all()
        Gtk.main()


class done(Gtk.Window):
    def __init__(self):
        super().__init__(title="DONE!")
        box = Gtk.Box(spacing=2)
        self.add(box)
        self.entry = Gtk.Entry()
        label = Gtk.Label(label="done")
        button1 = Gtk.Button("Exit")
        button1.connect("clicked", exit)
        box.pack_start(label, True, True, 0)
        box.pack_start(button1, True, True, 0)

    def exit():
        Gtk.main_quit()


win = main()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
